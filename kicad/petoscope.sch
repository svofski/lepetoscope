EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L esp12f:ESP12F U3
U 1 1 61880B80
P 7450 2850
F 0 "U3" H 7500 4009 47  0000 C CNN
F 1 "ESP12F" H 7500 3922 47  0000 C CNN
F 2 "ESP8266:ESP-12E_SMD" H 7450 2850 47  0001 C CNN
F 3 "" H 7450 2850 47  0001 C CNN
	1    7450 2850
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C1
U 1 1 618821FA
P 1450 3850
F 0 "C1" H 1542 3896 50  0000 L CNN
F 1 "4.7u" H 1542 3805 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1450 3850 50  0001 C CNN
F 3 "~" H 1450 3850 50  0001 C CNN
	1    1450 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3650 1450 3650
Wire Wire Line
	1450 3650 1450 3750
Wire Wire Line
	1450 3950 1450 4050
$Comp
L power:GND #PWR03
U 1 1 61883648
P 1450 4050
F 0 "#PWR03" H 1450 3800 50  0001 C CNN
F 1 "GND" H 1455 3877 50  0001 C CNN
F 2 "" H 1450 4050 50  0001 C CNN
F 3 "" H 1450 4050 50  0001 C CNN
	1    1450 4050
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C3
U 1 1 618846C5
P 3450 3900
F 0 "C3" H 3542 3946 50  0000 L CNN
F 1 "4.7u" H 3542 3855 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3450 3900 50  0001 C CNN
F 3 "~" H 3450 3900 50  0001 C CNN
	1    3450 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3650 3450 3800
$Comp
L power:GND #PWR06
U 1 1 61885071
P 3450 4050
F 0 "#PWR06" H 3450 3800 50  0001 C CNN
F 1 "GND" H 3455 3877 50  0001 C CNN
F 2 "" H 3450 4050 50  0001 C CNN
F 3 "" H 3450 4050 50  0001 C CNN
	1    3450 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4000 3450 4050
Wire Wire Line
	3450 3650 3950 3650
Connection ~ 3450 3650
$Comp
L device:R_Small R13
U 1 1 61885CDA
P 9750 2600
F 0 "R13" V 9700 2450 50  0000 C CNN
F 1 "470" V 9800 2450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 9750 2600 50  0001 C CNN
F 3 "~" H 9750 2600 50  0001 C CNN
	1    9750 2600
	0    1    1    0   
$EndComp
$Comp
L device:R_Small R15
U 1 1 618863FE
P 10000 2700
F 0 "R15" V 10050 2550 50  0000 C CNN
F 1 "470" V 10050 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 10000 2700 50  0001 C CNN
F 3 "~" H 10000 2700 50  0001 C CNN
	1    10000 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 2600 9650 2600
Wire Wire Line
	9250 2700 9900 2700
Wire Wire Line
	9850 2600 10350 2600
Wire Wire Line
	10100 2700 10350 2700
$Comp
L device:R_Small R7
U 1 1 61886D19
P 3900 2500
F 0 "R7" H 3841 2454 50  0000 R CNN
F 1 "220K" H 3841 2545 50  0000 R CNN
F 2 "Resistors_SMD:R_0805" H 3900 2500 50  0001 C CNN
F 3 "~" H 3900 2500 50  0001 C CNN
	1    3900 2500
	-1   0    0    1   
$EndComp
$Comp
L device:R_Small R8
U 1 1 61887B8F
P 3900 2850
F 0 "R8" H 3841 2804 50  0000 R CNN
F 1 "100K" H 3841 2895 50  0000 R CNN
F 2 "Resistors_SMD:R_0805" H 3900 2850 50  0001 C CNN
F 3 "~" H 3900 2850 50  0001 C CNN
	1    3900 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 2600 3900 2700
Connection ~ 3900 2700
Wire Wire Line
	3900 2700 3900 2750
Wire Wire Line
	3900 2400 3900 2300
$Comp
L power:GND #PWR013
U 1 1 61888674
P 3900 3000
F 0 "#PWR013" H 3900 2750 50  0001 C CNN
F 1 "GND" H 3905 2827 50  0001 C CNN
F 2 "" H 3900 3000 50  0001 C CNN
F 3 "" H 3900 3000 50  0001 C CNN
	1    3900 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2950 3900 3000
Text Label 3900 2300 0    50   ~ 0
ADC3V3
Text Label 4200 2700 2    50   ~ 0
ADC1V
$Comp
L device:CP_Small C4
U 1 1 6188989D
P 3950 3900
F 0 "C4" H 4038 3946 50  0000 L CNN
F 1 "220uF" H 4038 3855 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-D_EIA-7343-31_Reflow" H 3950 3900 50  0001 C CNN
F 3 "~" H 3950 3900 50  0001 C CNN
	1    3950 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3800 3950 3650
Connection ~ 3950 3650
Wire Wire Line
	3950 3650 4100 3650
$Comp
L power:GND #PWR08
U 1 1 6188A130
P 3950 4050
F 0 "#PWR08" H 3950 3800 50  0001 C CNN
F 1 "GND" H 3955 3877 50  0001 C CNN
F 2 "" H 3950 4050 50  0001 C CNN
F 3 "" H 3950 4050 50  0001 C CNN
	1    3950 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 4000 3950 4050
$Comp
L device:R_Small R14
U 1 1 6188A867
P 9750 3000
F 0 "R14" V 9700 3150 50  0000 C CNN
F 1 "12K" V 9700 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 9750 3000 50  0001 C CNN
F 3 "~" H 9750 3000 50  0001 C CNN
	1    9750 3000
	0    1    1    0   
$EndComp
$Comp
L device:R_Small R16
U 1 1 6188B45F
P 10000 3100
F 0 "R16" V 10000 3550 50  0000 C CNN
F 1 "12K" V 10000 3350 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 10000 3100 50  0001 C CNN
F 3 "~" H 10000 3100 50  0001 C CNN
	1    10000 3100
	0    1    1    0   
$EndComp
$Comp
L device:R_Small R11
U 1 1 6188B931
P 5300 2800
F 0 "R11" V 5400 2900 50  0000 C CNN
F 1 "12K" V 5400 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 5300 2800 50  0001 C CNN
F 3 "~" H 5300 2800 50  0001 C CNN
	1    5300 2800
	0    -1   1    0   
$EndComp
$Comp
L device:R_Small R10
U 1 1 6188BBBF
P 5050 2600
F 0 "R10" V 4950 2500 50  0000 C CNN
F 1 "12K" V 4950 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 5050 2600 50  0001 C CNN
F 3 "~" H 5050 2600 50  0001 C CNN
	1    5050 2600
	0    1    1    0   
$EndComp
$Comp
L device:R_Small R12
U 1 1 6188BF92
P 9600 3450
F 0 "R12" H 9450 3400 50  0000 C CNN
F 1 "3K3" H 9450 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 9600 3450 50  0001 C CNN
F 3 "~" H 9600 3450 50  0001 C CNN
	1    9600 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	9250 3000 9350 3000
Wire Wire Line
	9850 3000 10150 3000
$Comp
L power:+3.3V #PWR024
U 1 1 6188D610
P 10200 3000
F 0 "#PWR024" H 10200 2850 50  0001 C CNN
F 1 "+3.3V" V 10200 3150 50  0000 L CNN
F 2 "" H 10200 3000 50  0001 C CNN
F 3 "" H 10200 3000 50  0001 C CNN
	1    10200 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 3100 9900 3100
Wire Wire Line
	10100 3100 10150 3100
Wire Wire Line
	10150 3100 10150 3000
Connection ~ 10150 3000
Wire Wire Line
	10150 3000 10200 3000
Wire Wire Line
	5750 2800 5400 2800
Wire Wire Line
	5200 2800 4900 2800
$Comp
L power:+3.3V #PWR015
U 1 1 6189114C
P 4850 2800
F 0 "#PWR015" H 4850 2650 50  0001 C CNN
F 1 "+3.3V" V 4850 2950 50  0000 L CNN
F 2 "" H 4850 2800 50  0001 C CNN
F 3 "" H 4850 2800 50  0001 C CNN
	1    4850 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 2600 5550 2600
Wire Wire Line
	4950 2600 4900 2600
Wire Wire Line
	4900 2600 4900 2800
Connection ~ 4900 2800
Wire Wire Line
	4900 2800 4850 2800
Wire Wire Line
	3900 2700 5750 2700
$Comp
L power:+3.3V #PWR010
U 1 1 61898BD4
P 4100 3650
F 0 "#PWR010" H 4100 3500 50  0001 C CNN
F 1 "+3.3V" V 4100 3800 50  0000 L CNN
F 2 "" H 4100 3650 50  0001 C CNN
F 3 "" H 4100 3650 50  0001 C CNN
	1    4100 3650
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR017
U 1 1 61899544
P 5550 3300
F 0 "#PWR017" H 5550 3150 50  0001 C CNN
F 1 "+3.3V" V 5550 3450 50  0000 L CNN
F 2 "" H 5550 3300 50  0001 C CNN
F 3 "" H 5550 3300 50  0001 C CNN
	1    5550 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 3300 5650 3300
$Comp
L device:C_Small C6
U 1 1 6189A3A1
P 5650 3500
F 0 "C6" H 5742 3546 50  0000 L CNN
F 1 "100n" H 5742 3455 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5650 3500 50  0001 C CNN
F 3 "~" H 5650 3500 50  0001 C CNN
	1    5650 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3400 5650 3300
Connection ~ 5650 3300
Wire Wire Line
	5650 3300 5550 3300
$Comp
L power:GND #PWR022
U 1 1 6189B03B
P 9250 3400
F 0 "#PWR022" H 9250 3150 50  0001 C CNN
F 1 "GND" H 9255 3227 50  0001 C CNN
F 2 "" H 9250 3400 50  0001 C CNN
F 3 "" H 9250 3400 50  0001 C CNN
	1    9250 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 3300 9250 3400
$Comp
L power:GND #PWR019
U 1 1 6189BE8B
P 5650 3650
F 0 "#PWR019" H 5650 3400 50  0001 C CNN
F 1 "GND" H 5655 3477 50  0001 C CNN
F 2 "" H 5650 3650 50  0001 C CNN
F 3 "" H 5650 3650 50  0001 C CNN
	1    5650 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3600 5650 3650
Wire Wire Line
	9600 3550 9600 3700
$Comp
L power:GND #PWR023
U 1 1 6189FEB6
P 9600 3700
F 0 "#PWR023" H 9600 3450 50  0001 C CNN
F 1 "GND" H 9605 3527 50  0001 C CNN
F 2 "" H 9600 3700 50  0001 C CNN
F 3 "" H 9600 3700 50  0001 C CNN
	1    9600 3700
	1    0    0    -1  
$EndComp
NoConn ~ 7200 4150
NoConn ~ 7300 4150
NoConn ~ 7400 4150
NoConn ~ 7500 4150
NoConn ~ 7600 4150
NoConn ~ 7700 4150
Wire Wire Line
	9250 2900 11100 2900
Wire Wire Line
	5750 3200 4700 3200
Text Label 4700 3200 0    50   ~ 0
OLED_MOSI
Wire Wire Line
	5750 3000 4700 3000
Text Label 4700 3000 0    50   ~ 0
OLED_CLK
Wire Wire Line
	9250 2800 11100 2800
Text Label 11100 2800 2    50   ~ 0
OLED_DC
Wire Wire Line
	9250 3200 9600 3200
Wire Wire Line
	9600 3200 9600 3350
Wire Wire Line
	9600 3200 11100 3200
Connection ~ 9600 3200
Text Label 11100 3200 2    50   ~ 0
OLED_CS
Text Label 11100 2900 2    50   ~ 0
OLED_RESET
$Comp
L conn:Conn_01x07_Female J6
U 1 1 618AC711
P 9500 5650
F 0 "J6" H 9528 5676 50  0000 L CNN
F 1 "OLED7PIN" H 9550 5550 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x07_Pitch2.54mm" H 9500 5650 50  0001 C CNN
F 3 "~" H 9500 5650 50  0001 C CNN
	1    9500 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 618B04A0
P 9200 5350
F 0 "#PWR020" H 9200 5100 50  0001 C CNN
F 1 "GND" H 9205 5177 50  0001 C CNN
F 2 "" H 9200 5350 50  0001 C CNN
F 3 "" H 9200 5350 50  0001 C CNN
	1    9200 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	9200 5350 9300 5350
$Comp
L power:+3.3V #PWR021
U 1 1 618B193F
P 9200 5450
F 0 "#PWR021" H 9200 5300 50  0001 C CNN
F 1 "+3.3V" V 9200 5600 50  0000 L CNN
F 2 "" H 9200 5450 50  0001 C CNN
F 3 "" H 9200 5450 50  0001 C CNN
	1    9200 5450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9200 5450 9300 5450
Wire Wire Line
	9300 5550 8850 5550
Wire Wire Line
	9300 5650 8850 5650
Wire Wire Line
	9300 5750 8850 5750
Wire Wire Line
	9300 5850 8850 5850
Wire Wire Line
	9300 5950 8850 5950
Text Label 8850 5550 0    50   ~ 0
OLED_CLK
Text Label 8850 5650 0    50   ~ 0
OLED_MOSI
Text Label 8850 5750 0    50   ~ 0
OLED_RESET
Text Label 8850 5850 0    50   ~ 0
OLED_DC
Text Label 8850 5950 0    50   ~ 0
OLED_CS
Connection ~ 1450 3650
$Comp
L conn:Conn_01x03 J1
U 1 1 618BD140
P 850 3650
F 0 "J1" H 768 3325 50  0000 C CNN
F 1 "PWR5V" H 768 3416 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03" H 850 3650 50  0001 C CNN
F 3 "~" H 850 3650 50  0001 C CNN
	1    850  3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 3650 1450 3650
Wire Wire Line
	1050 3550 1150 3550
Wire Wire Line
	1150 3550 1150 3750
Wire Wire Line
	1150 3750 1050 3750
Wire Wire Line
	1150 3750 1150 3850
Connection ~ 1150 3750
$Comp
L power:GND #PWR01
U 1 1 618C12FD
P 1150 3850
F 0 "#PWR01" H 1150 3600 50  0001 C CNN
F 1 "GND" H 1155 3677 50  0001 C CNN
F 2 "" H 1150 3850 50  0001 C CNN
F 3 "" H 1150 3850 50  0001 C CNN
	1    1150 3850
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x06_Female J4
U 1 1 618C1A73
P 2850 2750
F 0 "J4" H 2878 2726 50  0000 L CNN
F 1 "SERIALDONG" H 2878 2635 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Angled_1x06" H 2850 2750 50  0001 C CNN
F 3 "~" H 2850 2750 50  0001 C CNN
	1    2850 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 618C2781
P 2400 2950
F 0 "#PWR05" H 2400 2700 50  0001 C CNN
F 1 "GND" H 2405 2777 50  0001 C CNN
F 2 "" H 2400 2950 50  0001 C CNN
F 3 "" H 2400 2950 50  0001 C CNN
	1    2400 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 2950 2400 2950
Wire Wire Line
	1450 3650 1450 3050
$Comp
L conn:Conn_01x02 J3
U 1 1 618CA835
P 1450 2850
F 0 "J3" V 1414 2662 50  0000 R CNN
F 1 "DONGPOWER" V 1323 2662 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1450 2850 50  0001 C CNN
F 3 "~" H 1450 2850 50  0001 C CNN
	1    1450 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1550 3050 2650 3050
Text Label 10350 2600 2    50   ~ 0
RxD
Text Label 10350 2700 2    50   ~ 0
TxD
Text Label 2300 2850 0    50   ~ 0
RxD
Wire Wire Line
	2650 2850 2300 2850
Wire Wire Line
	2650 2750 2300 2750
Text Label 2300 2750 0    50   ~ 0
TxD
Text Label 2300 3050 0    50   ~ 0
5VDONG
Text Label 1250 3650 0    50   ~ 0
5V
$Comp
L linear:LM358 U2
U 1 1 618D5AA2
P 4350 6350
F 0 "U2" H 4700 6250 50  0000 L CNN
F 1 "LM358" H 4700 6100 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 4350 6350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 4350 6350 50  0001 C CNN
	1    4350 6350
	1    0    0    -1  
$EndComp
$Comp
L device:R_Small R9
U 1 1 618D719C
P 4500 6900
F 0 "R9" V 4696 6900 50  0000 C CNN
F 1 "100K" V 4605 6900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 4500 6900 50  0001 C CNN
F 3 "~" H 4500 6900 50  0001 C CNN
	1    4500 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 6350 4650 6900
Wire Wire Line
	4650 6900 4600 6900
Wire Wire Line
	4400 6900 4000 6900
Wire Wire Line
	4000 6900 4000 6450
Wire Wire Line
	4000 6450 4050 6450
$Comp
L device:R_Small R5
U 1 1 618DAAAE
P 4000 7100
F 0 "R5" H 4059 7146 50  0000 L CNN
F 1 "100K" H 4059 7055 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 4000 7100 50  0001 C CNN
F 3 "~" H 4000 7100 50  0001 C CNN
	1    4000 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 7000 4000 6900
Connection ~ 4000 6900
$Comp
L power:GND #PWR011
U 1 1 618DC7E6
P 4000 7250
F 0 "#PWR011" H 4000 7000 50  0001 C CNN
F 1 "GND" H 4005 7077 50  0001 C CNN
F 2 "" H 4000 7250 50  0001 C CNN
F 3 "" H 4000 7250 50  0001 C CNN
	1    4000 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 7200 4000 7250
$Comp
L power:GND #PWR012
U 1 1 618DE664
P 4250 6650
F 0 "#PWR012" H 4250 6400 50  0001 C CNN
F 1 "GND" H 4255 6477 50  0001 C CNN
F 2 "" H 4250 6650 50  0001 C CNN
F 3 "" H 4250 6650 50  0001 C CNN
	1    4250 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 6050 4250 5900
Text Label 4250 5350 0    50   ~ 0
5V
$Comp
L device:C_Small C5
U 1 1 618E1123
P 4600 6000
F 0 "C5" H 4692 6046 50  0000 L CNN
F 1 "100n" H 4692 5955 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4600 6000 50  0001 C CNN
F 3 "~" H 4600 6000 50  0001 C CNN
	1    4600 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5900 4250 5900
Wire Wire Line
	4250 5900 4250 5750
Connection ~ 4250 5900
$Comp
L power:GND #PWR014
U 1 1 618E4D5B
P 4600 6100
F 0 "#PWR014" H 4600 5850 50  0001 C CNN
F 1 "GND" H 4605 5927 50  0001 C CNN
F 2 "" H 4600 6100 50  0001 C CNN
F 3 "" H 4600 6100 50  0001 C CNN
	1    4600 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6350 5350 6350
Connection ~ 4650 6350
$Comp
L device:C_Small C2
U 1 1 618E6F50
P 2850 6250
F 0 "C2" V 2621 6250 50  0000 C CNN
F 1 "1u" V 2712 6250 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" H 2850 6250 50  0001 C CNN
F 3 "~" H 2850 6250 50  0001 C CNN
	1    2850 6250
	0    1    1    0   
$EndComp
$Comp
L device:R_Small R1
U 1 1 618E7815
P 2250 6100
F 0 "R1" V 2054 6100 50  0000 C CNN
F 1 "100K" V 2145 6100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 2250 6100 50  0001 C CNN
F 3 "~" H 2250 6100 50  0001 C CNN
	1    2250 6100
	0    1    1    0   
$EndComp
$Comp
L device:R_Small R2
U 1 1 618E7F96
P 2250 6450
F 0 "R2" V 2054 6450 50  0000 C CNN
F 1 "100K" V 2145 6450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 2250 6450 50  0001 C CNN
F 3 "~" H 2250 6450 50  0001 C CNN
	1    2250 6450
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 6100 2450 6100
Wire Wire Line
	2450 6100 2450 6250
Wire Wire Line
	2450 6450 2350 6450
Wire Wire Line
	2750 6250 2450 6250
Connection ~ 2450 6250
Wire Wire Line
	2450 6250 2450 6450
$Comp
L device:R_Small R4
U 1 1 618EE909
P 3600 6250
F 0 "R4" V 3404 6250 50  0000 C CNN
F 1 "100K" V 3495 6250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 3600 6250 50  0001 C CNN
F 3 "~" H 3600 6250 50  0001 C CNN
	1    3600 6250
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 6250 3700 6250
$Comp
L device:R_POT RV1
U 1 1 618F6474
P 3150 5550
F 0 "RV1" V 2943 5550 50  0000 C CNN
F 1 "100K" V 3034 5550 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Bourns_3296W_3-8Zoll_Inline_ScrewUp" H 3150 5550 50  0001 C CNN
F 3 "~" H 3150 5550 50  0001 C CNN
	1    3150 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 5550 3450 5550
Wire Wire Line
	3450 5550 3450 5650
$Comp
L power:GND #PWR09
U 1 1 618F957F
P 3450 5650
F 0 "#PWR09" H 3450 5400 50  0001 C CNN
F 1 "GND" H 3455 5477 50  0001 C CNN
F 2 "" H 3450 5650 50  0001 C CNN
F 3 "" H 3450 5650 50  0001 C CNN
	1    3450 5650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR07
U 1 1 618F9882
P 2600 5550
F 0 "#PWR07" H 2600 5400 50  0001 C CNN
F 1 "+3.3V" V 2600 5700 50  0000 L CNN
F 2 "" H 2600 5550 50  0001 C CNN
F 3 "" H 2600 5550 50  0001 C CNN
	1    2600 5550
	0    -1   -1   0   
$EndComp
$Comp
L device:R_Small R6
U 1 1 618FEB75
P 4250 5650
F 0 "R6" V 4150 5550 50  0000 C CNN
F 1 "100" V 4150 5700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 4250 5650 50  0001 C CNN
F 3 "~" H 4250 5650 50  0001 C CNN
	1    4250 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	4250 5550 4250 5350
Text Label 5350 6350 2    50   ~ 0
ADC3V3
$Comp
L conn:Conn_01x03 J2
U 1 1 6195D3E0
P 1450 6250
F 0 "J2" H 1368 5925 50  0000 C CNN
F 1 "AUDIO_IN" H 1368 6016 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03_Pitch2.54mm" H 1450 6250 50  0001 C CNN
F 3 "~" H 1450 6250 50  0001 C CNN
	1    1450 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 6150 1650 6100
Wire Wire Line
	1650 6100 2150 6100
Wire Wire Line
	1650 6350 1650 6450
Wire Wire Line
	1650 6450 2150 6450
$Comp
L power:GND #PWR02
U 1 1 61963A5E
P 1650 6250
F 0 "#PWR02" H 1650 6000 50  0001 C CNN
F 1 "GND" H 1655 6077 50  0001 C CNN
F 2 "" H 1650 6250 50  0001 C CNN
F 3 "" H 1650 6250 50  0001 C CNN
	1    1650 6250
	0    -1   -1   0   
$EndComp
Text Label 3150 6250 0    50   ~ 0
1.6V
Wire Wire Line
	5550 2600 5550 1700
Connection ~ 5550 2600
Wire Wire Line
	5550 2600 5750 2600
$Comp
L conn:Conn_01x04 J5
U 1 1 61969593
P 5450 1500
F 0 "J5" V 5414 1212 50  0000 R CNN
F 1 "Conn_01x04" V 5323 1212 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04" H 5450 1500 50  0001 C CNN
F 3 "~" H 5450 1500 50  0001 C CNN
	1    5450 1500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 6196B277
P 5650 1700
F 0 "#PWR018" H 5650 1450 50  0001 C CNN
F 1 "GND" H 5655 1527 50  0001 C CNN
F 2 "" H 5650 1700 50  0001 C CNN
F 3 "" H 5650 1700 50  0001 C CNN
	1    5650 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 3000 9350 1250
Wire Wire Line
	9350 1250 5150 1250
Wire Wire Line
	5150 1250 5150 1850
Wire Wire Line
	5150 1850 5450 1850
Wire Wire Line
	5450 1850 5450 1700
Connection ~ 9350 3000
Wire Wire Line
	9350 3000 9650 3000
$Comp
L power:GND #PWR016
U 1 1 6196EF08
P 5350 1700
F 0 "#PWR016" H 5350 1450 50  0001 C CNN
F 1 "GND" H 5355 1527 50  0001 C CNN
F 2 "" H 5350 1700 50  0001 C CNN
F 3 "" H 5350 1700 50  0001 C CNN
	1    5350 1700
	1    0    0    -1  
$EndComp
Text Notes 5600 1400 0    50   ~ 0
RST
Text Notes 5250 1400 0    50   ~ 0
FLASH
Wire Wire Line
	2950 6250 3150 6250
$Comp
L device:R_Small R3
U 1 1 61970002
P 3150 5950
F 0 "R3" H 3091 5904 50  0000 R CNN
F 1 "100K" H 3091 5995 50  0000 R CNN
F 2 "Resistors_SMD:R_0805" H 3150 5950 50  0001 C CNN
F 3 "~" H 3150 5950 50  0001 C CNN
	1    3150 5950
	-1   0    0    1   
$EndComp
Wire Wire Line
	3150 5850 3150 5700
Wire Wire Line
	3150 6050 3150 6250
Connection ~ 3150 6250
Wire Wire Line
	3150 6250 3500 6250
$Comp
L petoscope-rescue:MC33269D-3.3G-MC33269D-3.3G U1
U 1 1 61979316
P 2500 3850
F 0 "U1" H 2500 4320 50  0000 C CNN
F 1 "MC33269D-3.3G" H 2500 4229 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 2500 3850 50  0001 L BNN
F 3 "" H 2500 3850 50  0001 C CNN
	1    2500 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3650 3200 3750
Wire Wire Line
	3200 3750 3200 3850
Connection ~ 3200 3750
Wire Wire Line
	3200 3850 3200 3950
Connection ~ 3200 3850
$Comp
L power:GND #PWR04
U 1 1 61983D25
P 3200 4450
F 0 "#PWR04" H 3200 4200 50  0001 C CNN
F 1 "GND" H 3205 4277 50  0001 C CNN
F 2 "" H 3200 4450 50  0001 C CNN
F 3 "" H 3200 4450 50  0001 C CNN
	1    3200 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4350 3200 4450
Wire Wire Line
	3200 3650 3450 3650
Connection ~ 3200 3650
NoConn ~ 1800 3850
NoConn ~ 1800 3950
$Comp
L device:R_Small R17
U 1 1 619C046F
P 2750 5550
F 0 "R17" V 2950 5600 50  0000 C CNN
F 1 "100" V 2850 5600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 2750 5550 50  0001 C CNN
F 3 "~" H 2750 5550 50  0001 C CNN
	1    2750 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2600 5550 2650 5550
Wire Wire Line
	2850 5550 2900 5550
$Comp
L device:C_Small C7
U 1 1 619C7CEE
P 2900 5650
F 0 "C7" H 2650 5650 50  0000 L CNN
F 1 "100n" H 2650 5550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2900 5650 50  0001 C CNN
F 3 "~" H 2900 5650 50  0001 C CNN
	1    2900 5650
	1    0    0    -1  
$EndComp
Connection ~ 2900 5550
Wire Wire Line
	2900 5550 3000 5550
$Comp
L power:GND #PWR025
U 1 1 619C8608
P 2900 5750
F 0 "#PWR025" H 2900 5500 50  0001 C CNN
F 1 "GND" H 2905 5577 50  0001 C CNN
F 2 "" H 2900 5750 50  0001 C CNN
F 3 "" H 2900 5750 50  0001 C CNN
	1    2900 5750
	1    0    0    -1  
$EndComp
Text Notes 1350 6350 2    50   ~ 0
AUDIO IN\n~~2V peak-to-peak
Text Notes 8100 6950 0    197  Italic 0
LE PÉTOSCOPE
Text Notes 8400 7100 0    50   ~ 0
svofski 2021 https://www.sensi.org/~~svo
Text Notes 2050 6900 0    50   ~ 0
set to approx 1.6V\nshould read as 512 by adc
Text Notes 700  7500 0    79   ~ 0
Analog Front-End\nBuilt only this if using NodeMCU or similar board
NoConn ~ 5750 2900
NoConn ~ 5750 3100
Wire Notes Line
	600  600  11150 600 
Wire Notes Line
	11150 600  11150 4950
Wire Notes Line
	11150 4950 600  4950
Wire Notes Line
	600  4950 600  600 
Wire Notes Line
	600  7600 600  5050
Text Notes 4500 5450 0    50   ~ 0
5V can be tapped from NodeMCU VIN pin on some versions.\nOther versions have a protection diode, in that case solder \na wire to the input of 3.3V LDO on the board itself.
Wire Notes Line
	600  5050 6850 5050
Wire Notes Line
	6850 5050 6850 7600
Wire Notes Line
	600  7600 6850 7600
Wire Notes Line
	3150 6300 3150 6950
Wire Notes Line
	3150 6950 2100 6950
Text Notes 8800 6250 0    50   ~ 0
7-pin 128x64 OLED display
Text Notes 9900 3600 0    50   ~ 0
* this resistor is usually \nspecced at 10K, \nbut 10K never works for me
Text Notes 750  1000 0    79   ~ 0
ESP12F boilerplate\nNot needed when building around NodeMCU
$EndSCHEMATC
