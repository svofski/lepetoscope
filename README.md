# Le Pétoscope

A handy minimalistic audio oscilloscope to be used together with [Le Pétomane](https://gitlab.com/svofski/lepetomane). Not rich in features but very easy to build and immensely useful when creating sounds on a synth. It can be built on a breadboard using parts that you probably have lying around, or assembled nicely as a PCB module. It uses ESP8266 for ADC and processing power, however WiFi features are not used.

![Le Pétoscope](pics/lepetoscope.jpg "Le Pétoscope in action")

Demo video: [youtube](https://www.youtube.com/watch?v=eriZHtzsGGw)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/eriZHtzsGGw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


## How to build

You can build it using a bare ESP12F or similar module, or you can use a NodeMCU-like development board. Consult the [schematic](petoscope.pdf) for details.

### Essential parts list:
  
  * ESP12F (bare module) or NodeMCU development board
  * 128x64 OLED display with SPI connection
  * LM358 opamp, a handful of 100K resistors and some common caps
  * one 100K trimpot

Build the hardware. I recommend setting the ADC voltage before connecting ESP12F. Upload the sketch. Connect to your sound output. Enjoy.

### Arduino settings

Board: NodeMCU (ESP12E - it doesn't matter if yours is ESP12F). CPU speed: 160MHz. You need to get u8g2 library using library manager.

You can build it on a breadboard. Or if you decide to make it a complete module, it would look like so:
![Le Pétoscope Parts Side](pics/lepetoscope-top.jpg "Le Pétoscope parts side view")
![Le Pétoscope Display Side](pics/lepetoscope-bot.jpg "Le Pétoscope display side view")
